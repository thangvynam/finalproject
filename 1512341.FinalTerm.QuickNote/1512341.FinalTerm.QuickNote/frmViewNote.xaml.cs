﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace _1512341.FinalTerm.QuickNote
{
    /// <summary>
    /// Interaction logic for frmViewNote.xaml
    /// </summary>
    public partial class frmViewNote : Window
    {
        List<string> listData = new List<string>();
        public frmViewNote()
        {
            InitializeComponent();
            
        }
        private void addItem(string text)
        {
            TextBlock block = new TextBlock();

            // Add the text   
            block.Text = text;

            // A little style...   
            block.Margin = new Thickness(2, 3, 2, 3);
            block.Cursor = Cursors.Hand;

            // Mouse events   
            block.MouseLeftButtonUp += (sender, e) =>
            {
                txtTags.Text = (sender as TextBlock).Text;
            };

            block.MouseEnter += (sender, e) =>
            {
                TextBlock b = sender as TextBlock;
                b.Background = Brushes.PeachPuff;
            };

            block.MouseLeave += (sender, e) =>
            {
                TextBlock b = sender as TextBlock;
                b.Background = Brushes.Transparent;
            };

            // Add to the panel   
            resultStack.Children.Add(block);
        }  

        

        private void save(object sender, RoutedEventArgs e)
        {
            if(!txtNote.Text.Equals("") && !txtTags.Text.Equals(""))
            {
                string temp = txtNote.Text.ToString().Replace("\r", " ");
                temp = temp.Replace("\n", " ");
                temp = temp.Trim();
                string u = "";
                for (var i = 0; i < temp.Count(); i++)
                {
                    if (i == 0)
                        u += temp.ElementAt(i);
                    else
                        u += temp.ElementAt(i);
                    if (temp.ElementAt(i) == ' ')
                    {
                        u += " ";

                        while (temp.ElementAt(i) == ' ')
                        {
                            i++;
                        }
                        u += temp.ElementAt(i);
                    }

                }
                if(txtTags.Text.Contains("%"))
                {

                    string[] arr = txtTags.Text.Split('%');
                    for(int i=0;i<arr.Count();i++)
                    {
                        string s = arr[i] + "^" + u;
                        FileFactory.writeFile(System.AppDomain.CurrentDomain.BaseDirectory + "/test.txt", s);
                    }
                }
                else
                {
                    
                    //string temp=txtNote.Text.ToString().Replace("\r\n\r\n", "");
                    //temp=temp.Replace("\r\n",""); // đề phòng trường hợp do không lường trước được HTML 
                    string s = txtTags.Text.ToString() + "^" + u;

                    FileFactory.writeFile(System.AppDomain.CurrentDomain.BaseDirectory + "/test.txt", s);
                }
                // cập lại Global.dic 
                Global.dic.Clear();
                List<string> temp1 = new List<string>();
                Dictionary<string, List<string>> dicTemp = new Dictionary<string, List<string>>();
                temp1 = FileFactory.readFile(System.AppDomain.CurrentDomain.BaseDirectory + "/test.txt");
                for (int i = 0; i < temp1.Count - 1; i += 2)
                {
                    //items.Add(new Item() { tags = temp.ElementAt(i), note = temp.ElementAt(i + 1) });
                    List<string> content = new List<string>();
                    if (temp1.ElementAt(i) == "*")
                        continue;
                    content.Add(temp1.ElementAt(i + 1));
                    for (int j = i + 2; j < temp1.Count; j += 2)
                    {
                        if (temp1.ElementAt(i).Equals(temp1.ElementAt(j)))
                        {
                            content.Add(temp1.ElementAt(j + 1));
                            temp1[j] = "*"; // đánh dấu 
                        }
                    }
                    dicTemp.Add(temp1.ElementAt(i), content);
                }
                Global.dic = dicTemp;
                MessageBox.Show("Saved successfully", "Notification", MessageBoxButton.OK, MessageBoxImage.Asterisk);

                txtNote.Text = "";
                txtTags.Text = "";
            }
            else
            {
                MessageBox.Show("Please , check again !", "Notification", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
      

        private void txtNote_down(object sender, KeyEventArgs e)
        {
            if (txtNote.Text.Equals("Nội dung được nhập ở đây ..."))
            {
                txtNote.Text = "";
                txtNote.Foreground = Brushes.Black;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Global.checkViewNote = false;
            e.Cancel = true;
            this.Hide();
        }

        private void txtTags_KeyUp(object sender, KeyEventArgs e)
        {
            bool found = false;
            var border = (resultStack.Parent as ScrollViewer).Parent as Border;
            
            foreach (KeyValuePair<string, List<string>> item in Global.dic)
            { 
                for(int i=0;i<item.Value.Count;i++)
                {
                    listData.Add(item.Key);
                } 
            }
            string query = (sender as TextBox).Text;

            if (query.Length == 0)
            {
                // Clear   
                resultStack.Children.Clear();
                border.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                border.Visibility = System.Windows.Visibility.Visible;
            }

            // Clear the list   
            resultStack.Children.Clear();

                // Add the result   
                foreach (var obj in listData)
                {
                    if (obj.ToLower().StartsWith(query.ToLower()))
                    {
                        // The word starts with this... Autocomplete must work   
                        addItem(obj);
                        found = true;
                    }
                }
                if (!found)
                {
                    resultStack.Children.Add(new TextBlock() { Text = "No results found." });
                }
            }

        }
}
