﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _1512341.FinalTerm.QuickNote
{
    public class FileFactory
    {
        public static bool writeFile(string path,string s)
        {
            try
            {
                StreamWriter sw = new StreamWriter(path, true,UnicodeEncoding.UTF8);
                sw.WriteLine(s);
                sw.Close();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return false;
        }

        public static List<string> readFile(string path)
        {
           
            List<string> tagNote = new List<string>();
            try
            {
                StreamReader sr = new StreamReader(path, Encoding.UTF8);
                string line =sr.ReadLine();
                while(line!=null)
                {
                    string[] arr = line.Split('^');
                    tagNote.Add(arr[0]);
                    tagNote.Add(arr[1]);
                    line = sr.ReadLine();
                }
                
                sr.Close();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return tagNote;
        }
    }
}
