﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;


namespace _1512341.FinalTerm.QuickNote
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        Hook keyboardHook = new Hook();
        
        public MainWindow()
        {
           
            InitializeComponent();
            
            this.Visibility = Visibility.Hidden;
            this.Show();
            this.Focus();
            keyboardHook.Install();
            string KEY_DOWN;
            keyboardHook.KeyDown += (sender, e) =>
            {

                KEY_DOWN = e.KeyCode + "";
                if (KEY_DOWN.Equals("F7"))
                {
                    if (!Global.checkOpenView)
                    {
                        frmOpenView frmOpenView = new frmOpenView();
                        frmOpenView.Show();
                        Global.checkOpenView = true;
                    }
                    else
                        MessageBox.Show("You are opening this window", "Quick note", MessageBoxButton.OK, MessageBoxImage.Error);
                    
                }
                else if (KEY_DOWN.Equals("F6"))
                {
                    if (!Global.checkViewNote)
                    {
                        frmViewNote frmViewNote = new frmViewNote();
                        frmViewNote.Show();
                        Global.checkViewNote = true;
                    }
                    else
                        MessageBox.Show("You are opening this window", "Quick note", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                
            };
           

        }
        private void load(object sender, RoutedEventArgs e)
        {

            System.Windows.Forms.NotifyIcon notifyIcon = new System.Windows.Forms.NotifyIcon();
            //string defaultFolder = System.AppDomain.CurrentDomain.BaseDirectory;
            
            //string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName)+ "\\note.ico";
            notifyIcon.Icon = _1512341.FinalTerm.QuickNote.Properties.Resources.note;
            notifyIcon.Visible = true;
            notifyIcon.ShowBalloonTip(500, "Notification", "App is running", System.Windows.Forms.ToolTipIcon.Info);
            

            System.Windows.Forms.ContextMenu contextMenu = new System.Windows.Forms.ContextMenu();
            contextMenu.MenuItems.Add("Open quick note (F6) ", new EventHandler(openNote));
            contextMenu.MenuItems.Add("View notes (F7)", new EventHandler(viewNotes));
            contextMenu.MenuItems.Add("View statistics", new EventHandler(viewStatistics));
            contextMenu.MenuItems.Add("Exit", new EventHandler(exit));

            notifyIcon.ContextMenu = contextMenu;

            List<string> temp = new List<string>();
            Dictionary<string, List<string>> dicTemp = new Dictionary<string, List<string>>();
            
            temp = FileFactory.readFile(System.AppDomain.CurrentDomain.BaseDirectory + "/test.txt");
            for (int i = 0; i < temp.Count - 1; i += 2)
            {
                //items.Add(new Item() { tags = temp.ElementAt(i), note = temp.ElementAt(i + 1) });
                List<string> content = new List<string>();
                if (temp.ElementAt(i) == "*")
                    continue;
                content.Add(temp.ElementAt(i + 1));
                for (int j = i + 2; j < temp.Count; j += 2)
                {
                    if (temp.ElementAt(i).Equals(temp.ElementAt(j)))
                    {
                        content.Add(temp.ElementAt(j + 1));
                        temp[j] = "*"; // đánh dấu 
                    }
                }
                dicTemp.Add(temp.ElementAt(i), content);
            }
            Global.dic = dicTemp;
            
        }

        private void openNote(object sender, EventArgs e)
        {
            frmViewNote frmViewNote = new frmViewNote();
            frmViewNote.Show();
        }
        private void viewStatistics(object sender, EventArgs e)
        {
            frmViewStatistic frmViewStatistic = new frmViewStatistic();
            frmViewStatistic.Show();
        }

        private void viewNotes(object sender, EventArgs e)
        {
            frmOpenView frmOpenView = new frmOpenView();
            frmOpenView.Show();
        }

        private void exit(object sender, EventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you want to close this app ? ", "Quick Note", MessageBoxButton.YesNo, MessageBoxImage.Information);
            if (result == MessageBoxResult.Yes)
                System.Windows.Application.Current.Shutdown();

        }
    }
}
