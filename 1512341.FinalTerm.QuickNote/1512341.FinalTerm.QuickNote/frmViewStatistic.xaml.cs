﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Controls.DataVisualization;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.DataVisualization.Charting;
namespace _1512341.FinalTerm.QuickNote
{
    /// <summary>
    /// Interaction logic for frmViewStatistic.xaml
    /// </summary>
    public partial class frmViewStatistic : Window
    {
        
        public frmViewStatistic()
        {
            
            InitializeComponent();
          
            Dictionary<string, int> chart = new Dictionary<string, int>();
            foreach (KeyValuePair<string, List<string>> item in Global.dic)
            {
                chart.Add(item.Key, item.Value.Count);
            }
            ((PieSeries)mcChart.Series[0]).ItemsSource = chart;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Chart pieChart = new Chart();
            
        }
    }
}
