﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace _1512341.FinalTerm.QuickNote
{
    /// <summary>
    /// Interaction logic for frmOpenView.xaml
    /// </summary>
    /// 
    
    public partial class frmOpenView : Window
    {


        
        public frmOpenView()
        {
            InitializeComponent();           
            List<Item> items = new List<Item>();

            foreach (KeyValuePair<string, List<string>> item in Global.dic)
            {
                items.Add(new Item() { tags = item.Key, countTags = item.Value.Count });
            }
            lstView.ItemsSource = items;
            
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Global.checkOpenView = false;
            e.Cancel = true;
            this.Hide();
        }

        private void lstView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            List<Content> listContent = new List<Content>();
            if(lstView.SelectedItems.Count>0)
            {
                Type t = lstView.SelectedItem.GetType();
                System.Reflection.PropertyInfo[] props = t.GetProperties();
                string propertyValue = props[0].GetValue(lstView.SelectedItem, null).ToString();
                foreach (KeyValuePair<string, List<string>> item in Global.dic)
                {
                   if(item.Key.ToString().Equals(propertyValue))
                   {
                       List<string> arr = item.Value;
                       for(int i=0;i<arr.Count;i++)
                       {
                           listContent.Add(new Content(){content = arr.ElementAt(i)});
                       }
                   }
                }
                lstContent.ItemsSource = listContent;
            }
        }

        private void lstContent_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lstContent.SelectedItems.Count > 0)
            {
                Type t = lstContent.SelectedItem.GetType();
                System.Reflection.PropertyInfo[] props = t.GetProperties();
                string propertyValue = props[0].GetValue(lstContent.SelectedItem, null).ToString();
                txtContent.Text = propertyValue;
            }
        }
    }
    public class Item
    {
        public string tags { get; set; }
        public int countTags { get; set; }
    }
    public class Content
    {
        public string content { get; set; }
    }
  
}
